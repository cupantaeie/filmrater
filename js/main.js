/*
This page will not change. It is finished. The program works. I got it from:
https://www.youtube.com/watch?v=GrycH6F-ksY
*/
$('form.ajax').on('submit', function() {

	var that = $(this),							// this refers to the form - 'form.ajax'
		url  = that.attr('action'),				// action is the action from the form that calls for contact.php
		type = that.attr('method'),			 	// says "type" here but really refers to method in this case POST
		data = {};								// this javascript variable is going to hold our data

	
	// Now, anything with a name in the form is of interest to us. So we are going to loop through and get all the "names"
	that.find('[name]').each(function(index, value) {
		
		console.log(value);

		var that = $(this),
			name = that.attr('name'),
			value = that.val();
		
		data[name] = value;
	});

	$.ajax({
		url: url,
		type: type,
		data: data,
		success: function(response){
			console.log(response);
		}
	});

	console.log(data);

	return false;
});