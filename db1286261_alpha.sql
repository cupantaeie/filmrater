-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Host: 172.16.4.38
-- Generation Time: Oct 04, 2016 at 02:23 AM
-- Server version: 5.0.83-community
-- PHP Version: 5.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db1286261_alpha`
--

-- --------------------------------------------------------

--
-- Table structure for table `alpha_users`
--

CREATE TABLE IF NOT EXISTS `alpha_users` (
  `id` int(11) NOT NULL auto_increment COMMENT 'Primary Key',
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT 'This is when user began',
  `population` int(25) NOT NULL default '1000',
  `credits` int(11) NOT NULL,
  `energy` int(11) NOT NULL,
  `darkmatter` int(11) NOT NULL,
  `titanium` int(11) NOT NULL,
  `iron` int(11) NOT NULL,
  `ship` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `bankdebit` int(11) NOT NULL default '0',
  `totaluserdebit` int(11) NOT NULL default '0',
  `alphaship` int(3) NOT NULL default '0',
  `betaship` int(3) NOT NULL default '0',
  `deltaship` int(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `alpha_users`
--

INSERT INTO `alpha_users` (`id`, `firstname`, `lastname`, `username`, `password`, `email`, `date`, `created`, `population`, `credits`, `energy`, `darkmatter`, `titanium`, `iron`, `ship`, `city`, `bankdebit`, `totaluserdebit`, `alphaship`, `betaship`, `deltaship`) VALUES
(4, 'Donal', 'O Domhnaill', 'hammerhead', '5f4dcc3b5aa765d61d8327deb882cf99', '', '2010-10-19', '2012-09-28 12:04:41', 1000, 2659, 18, 21, 2, 2, 2, 11, 14193, 14432, 1, 0, 1),
(7, 'Linda', 'Reilly', 'bind', 'f150776d32dc1e993ace8020e01f75c9', '', '2010-11-27', '2010-11-27 01:42:47', 100, 35000, 5, 20, 4, 2, 1, 1, 0, 0, 1, 0, 1),
(13, 'Richard', 'Russell', 'stranger', 'b6281b62d763ff8af75d905dd5c0cdb0', '', '2011-01-01', '2011-01-01 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(14, 'sean', 'o domhnaill', 'johnnyfireball', 'c263fe815cf98d6e8ea5095adae9f503', '', '2011-01-02', '2011-01-02 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(15, 'Wallace', 'Jones', 'wallace', '953f2a1ec16efa816d7505148c2208b9', '', '2011-01-02', '2011-01-02 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(16, 'Ryan', 'Walker', 'crwalker', 'fb160fcf6f085a8da8ec92d2ed807507', '', '2011-01-02', '2011-01-02 22:03:33', 100, 574, 5, 20, 3, 2, 1, 1, 1074, 1074, 1, 0, 0),
(17, 'amanda', 'smith', 'asmith', '5f4dcc3b5aa765d61d8327deb882cf99', 'smith@itworks.ie', '2011-01-27', '2011-01-27 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(18, 'Mark', 'Aherne', 'faerdan', '5f4dcc3b5aa765d61d8327deb882cf99', 'faerdan@gmail.com', '2011-01-27', '2011-01-27 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(19, 'Andrew', 'Dewdney ', 'armour', 'bc124978ded2cf4da7cd25a6465e61e6', 'andrew@armour.ie', '2011-02-05', '2011-02-05 07:51:05', 100, 1979, 5, 20, 2, 2, 1, 1, -22, 422, 1, 0, 0),
(20, 'thingy', 'asdf', 'name', '5f4dcc3b5aa765d61d8327deb882cf99', 'asdf@sdf.ie', '2011-02-07', '2011-02-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(21, 'Jamie', 'Casey', 'hespy', 'e8750ee348987fead337a9928f238a79', 'jamiekc@gmail.com', '2011-02-07', '2011-02-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(22, 'God', 'Almighty', 'god', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'some@thing.com', '2011-02-07', '2011-02-07 16:44:12', 100, 95, 5, 20, 2, 2, 1, 1, -1905, 2305, 1, 0, 0),
(23, 'Mart', 'ODonnell', 'atheos', '0d107d09f5bbe40cade3de5c71e9e9b7', 'martinod@gmail.com', '2011-03-18', '2011-03-18 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(24, 'Accuffelerius', 'Accuffelerius', 'accuffelerius', '27fa77b668b5af3bc1b6a5f54aee3fa5', 'b.orizzzy@gmail.com', '2011-05-02', '2011-05-01 23:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(25, 'chivers', 'jam', 'strawberry', '5f4dcc3b5aa765d61d8327deb882cf99', 'donalod@gmail.com', '2012-03-07', '2012-03-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(26, 'John', 'Doe', 'johndoe', '5f4dcc3b5aa765d61d8327deb882cf99', 'nothanks', '2012-03-12', '2012-03-12 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(27, 'DoctorFranko', 'DoctorFrankoED', 'doctorfranko', 'cca457c541ed69d735611fed2522b388', 'docrorleonel776@gmail.com', '2013-02-18', '2013-02-18 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(28, 'DoctorFederic', 'DoctorFedericZA', 'doctorfederic', '06a960a95853eb9816ff2d1f983aaf98', 'doctorfederic@gmail.com', '2013-02-19', '2013-02-19 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(29, 'LeonelDoctor', 'LeonelDoctorPD', 'leoneldoctor', 'bd374eb6791a8b06c89db58c31ce338e', 'leoneldoctorsks7@gmail.com', '2013-02-20', '2013-02-20 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(30, 'cigioclop', 'cigioclopLM', 'cigioclop', '11061ffa0983aec3bcea630195c69a62', 'wroxerserge@hotmail.com', '2013-05-13', '2013-05-12 23:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL auto_increment,
  `firstname` varchar(20) NOT NULL,
  `secondname` varchar(20) NOT NULL,
  `addr1` varchar(20) NOT NULL,
  `addr2` varchar(20) NOT NULL,
  `addr3` varchar(20) NOT NULL,
  `addr4` varchar(20) NOT NULL,
  `phone1` varchar(18) NOT NULL,
  `phone2` varchar(18) NOT NULL,
  `issuetype` int(2) NOT NULL,
  `issuedetail` text NOT NULL,
  `paidyesno` int(2) NOT NULL,
  `howmuchtopay` int(7) NOT NULL,
  `paidsofar` int(7) NOT NULL,
  `firstcontactname` varchar(20) NOT NULL,
  `created` date NOT NULL,
  `modified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `firstname`, `secondname`, `addr1`, `addr2`, `addr3`, `addr4`, `phone1`, `phone2`, `issuetype`, `issuedetail`, `paidyesno`, `howmuchtopay`, `paidsofar`, `firstcontactname`, `created`, `modified`) VALUES
(1, 'Sarah', 'Jane', 'Mirror House', 'Carraroe', 'Co Galway', 'Ireland', '(087) 2716947', '(091)595207', 1, 'The computer needs more ram', 0, 76, 24, 'Martin O Donnell', '2013-10-12', '2013-10-18 19:47:24'),
(2, 'Jimmy Hendrix', '0', '', '', '', '', '234234234', '0', 0, '', 0, 0, 0, '', '0000-00-00', '2013-10-18 19:43:51'),
(3, 'Sam Goodman', '0', '', '', '', '', '23345234', '0', 0, '', 0, 0, 0, '', '0000-00-00', '2013-10-18 19:43:51'),
(4, 'Tome Watson', '0', '', '', '', '', '234234234', '0', 0, '', 0, 0, 0, '', '0000-00-00', '2013-10-18 19:43:51'),
(5, 'linda', '', '', '', '', '', '', '', 0, '', 0, 0, 0, '', '0000-00-00', '2013-10-19 01:05:31'),
(6, 'jimmy', 'sadfasd', '', '', '', '', '', '', 0, '', 0, 0, 0, '', '0000-00-00', '2013-10-19 01:35:22'),
(7, 'Linda', 'Reilly', '14 Foxfield', 'Casttttt', 'Garryduff', 'Mayo', '(091)234234', '(087)3234234', 1, 'Computer is broken', 1, 3453, 23, 'Mart', '0000-00-00', '2013-10-19 01:36:05'),
(8, 'Sophir', 'Ni Domhnaill', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-19 01:34:42'),
(9, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(10, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(11, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(12, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(13, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(14, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(15, 'Sophir', 'Ni D', 'here', 'there', 'and', 'everythwer', '999999', '8888888', 2, 'She is a lovely girl and she is smart', 1, 1231, 44, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(16, 'dino', 'sawe', 'asdf', 'asdf', 'asdf', 'asdf', '99999', '111111', 1, '2', 1, 1, 1, 'Steve Jobs', '2013-10-19', '2013-10-18 23:00:00'),
(17, 'Nora', 'Conney', 'The Island', 'y', 'y', 'y', '(091)5555555', '(091)5555555', 1, 'Nora was saying something about her phone i think.', 1, 35, 0, 'Donal', '2013-10-19', '2013-10-18 23:00:00'),
(18, 'john', 'smith', 'lkj', 'lkj', 'lkj', 'lkj', '98098', '098098', 1, 'this is very easyt to use', 1, 67, 5, 'Mart', '2013-10-19', '2013-10-18 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `filmrater`
--

CREATE TABLE IF NOT EXISTS `filmrater` (
  `id` int(11) NOT NULL auto_increment COMMENT 'Auto Increment',
  `film_title` varchar(250) NOT NULL COMMENT 'Stores name of the film.',
  `imdb_ref` varchar(20) NOT NULL COMMENT 'This is the tt that imdb has',
  `json_chunk` varchar(10000) NOT NULL COMMENT 'This is where all of the information about the films is going to be',
  `contributer` varchar(50) NOT NULL COMMENT 'Name of person that added this film',
  `score` int(3) NOT NULL COMMENT 'Percent score film has from contributer',
  `date_created` date NOT NULL COMMENT 'Date the field was created (that film was added)',
  `date_modified` date NOT NULL COMMENT 'Date that this field was last changed',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `galaxy`
--

CREATE TABLE IF NOT EXISTS `galaxy` (
  `id` int(11) NOT NULL auto_increment,
  `population` int(11) NOT NULL,
  `worldcredits` int(11) NOT NULL,
  `loanedamount` int(11) NOT NULL default '0',
  `repayedloadamount` int(11) NOT NULL default '0',
  `interestrate` int(5) NOT NULL default '5',
  `darkmatter` int(11) NOT NULL,
  `titanium` int(11) NOT NULL,
  `iron` int(11) NOT NULL,
  `dmprice` int(11) NOT NULL,
  `tiprice` int(11) NOT NULL,
  `irprice` int(11) NOT NULL,
  `darkmatterqty` int(11) NOT NULL,
  `titaniumqty` int(11) NOT NULL,
  `ironqty` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `galaxy`
--

INSERT INTO `galaxy` (`id`, `population`, `worldcredits`, `loanedamount`, `repayedloadamount`, `interestrate`, `darkmatter`, `titanium`, `iron`, `dmprice`, `tiprice`, `irprice`, `darkmatterqty`, `titaniumqty`, `ironqty`) VALUES
(1, 1000, 2147483647, 20878, 2453, 5, 81, 4991, 9810, 5000, 2500, 100, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oscars_users`
--

CREATE TABLE IF NOT EXISTS `oscars_users` (
  `id` int(4) NOT NULL auto_increment,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `nom1` varchar(100) NOT NULL default '0',
  `nom2` varchar(100) NOT NULL default '0',
  `nom3` varchar(100) NOT NULL default '0',
  `nom4` varchar(100) NOT NULL default '0',
  `nom5` varchar(100) NOT NULL default '0',
  `nom6` varchar(100) NOT NULL default '0',
  `nom7` varchar(100) NOT NULL default '0',
  `nom8` varchar(100) NOT NULL default '0',
  `nom9` varchar(100) NOT NULL default '0',
  `nom10` varchar(100) NOT NULL default '0',
  `NomFlag` tinyint(1) NOT NULL,
  `Created` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=105 ;

--
-- Dumping data for table `oscars_users`
--

INSERT INTO `oscars_users` (`id`, `name`, `email`, `nom1`, `nom2`, `nom3`, `nom4`, `nom5`, `nom6`, `nom7`, `nom8`, `nom9`, `nom10`, `NomFlag`, `Created`) VALUES
(91, 'Gerry Campbell', 'Enter Your Email', 'The Revenant', 'Adam McKay â€“ The Big Short', 'Leonardo DiCaprio â€“ The Revenant as Hugh Glass', 'Saoirse Ronan â€“ Brooklyn as Eilis Lacey', 'Mark Rylance â€“ Bridge of Spies as Rudolf Abel', 'Jennifer Jason Leigh â€“ The Hateful Eight as Daisy Domergue', 'Inside Out', 'Star Wars: The Force Awakens â€“ John Williams', 'The Martian', 'The Big Short â€“ Adam McKay and Charles Randolph from The Big Short by Michael Lewis', 0, '2016-02-22'),
(93, 'yllnora', 'yllnora.elshanii@gmail.com', 'The Revenant', 'Alejandro G. IÃ±Ã¡rritu â€“ The Revenant', 'Leonardo DiCaprio â€“ The Revenant as Hugh Glass', 'Jennifer Lawrence â€“ Joy as Joy Mangano', 'Sylvester Stallone â€“ Creed as Rocky Balboa', 'Kate Winslet â€“ Steve Jobs as Joanna Hoffman', 'Inside Out', 'Star Wars: The Force Awakens â€“ John Williams', 'Ex Machina', 'Room â€“ Emma Donoghue from Room by Emma Donoghue', 0, '2016-02-23'),
(94, 'Finbarr', 'thebionn@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-23'),
(95, 'Barry keane', 'Keanester@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-23'),
(96, 'Wallace Jones', 'heywriteus@hotmail.com', 'The Martian', 'Alejandro G. IÃ±Ã¡rritu â€“ The Revenant', 'Leonardo DiCaprio â€“ The Revenant as Hugh Glass', 'Saoirse Ronan â€“ Brooklyn as Eilis Lacey', 'Tom Hardy â€“ The Revenant as John Fitzgerald', 'Alicia Vikander â€“ The Danish Girl as Gerda Wegener', 'Inside Out', 'Star Wars: The Force Awakens â€“ John Williams', 'Star Wars: The Force Awakens', 'The Martian â€“ Drew Goddard from The Martian by Andy Weir', 0, '2016-02-23'),
(97, 'Sarah Walker', 'sadgorilla@gmail.com', 'Room', 'Lenny Abrahamson â€“ Room', 'Eddie Redmayne â€“ The Danish Girl as Lili Elbe / Einar Wegener', 'Charlotte Rampling â€“ 45 Years as Kate Mercer', 'Sylvester Stallone â€“ Creed as Rocky Balboa', 'Jennifer Jason Leigh â€“ The Hateful Eight as Daisy Domergue', 'Inside Out', 'The Hateful Eight â€“ Ennio Morricone', 'Mad Max: Fury Road', 'Room â€“ Emma Donoghue from Room by Emma Donoghue', 0, '2016-02-23'),
(98, 'Sausage ', 'Sausage.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-24'),
(99, 'John Reilly', 'johnfreilly@hotmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-26'),
(100, 'Finola', 'Finolareilly@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-28'),
(101, 'Mark', 'Markgsmith64@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-28'),
(102, 'Paul', 'pauldreilly600@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-28'),
(103, 'Linda', 'linreilly@gmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-02-29'),
(104, 'JimmiXS', 'jimos4581rt@hotmail.com', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 0, '2016-08-13');

-- --------------------------------------------------------

--
-- Table structure for table `ships`
--

CREATE TABLE IF NOT EXISTS `ships` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(11) NOT NULL,
  `shipclass` varchar(11) NOT NULL,
  `speed` int(11) NOT NULL default '140',
  `capacity` int(11) NOT NULL default '5',
  `crew` int(11) NOT NULL default '1',
  `guns` int(11) NOT NULL default '0',
  `credits` int(11) NOT NULL default '100',
  `iron` int(11) NOT NULL default '5',
  `titanium` int(11) NOT NULL default '1',
  `dm` int(11) NOT NULL default '0',
  `energy` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ships`
--

INSERT INTO `ships` (`id`, `username`, `shipclass`, `speed`, `capacity`, `crew`, `guns`, `credits`, `iron`, `titanium`, `dm`, `energy`) VALUES
(1, '', 'Delta', 140, 20000, 1, 2, 1000, 900, 14, 1, 1),
(2, '', 'Beta', 730, 67000, 3, 3, 7730, 1700, 30, 2, 6),
(3, '', 'Alpha', 2300, 270000, 47, 13, 53000, 5223, 120, 12, 430);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment COMMENT 'Primary Key',
  `firstname` varchar(25) NOT NULL,
  `lastname` varchar(25) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP COMMENT 'This is when user began',
  `population` int(25) NOT NULL default '1000',
  `credits` int(11) NOT NULL,
  `energy` int(11) NOT NULL,
  `darkmatter` int(11) NOT NULL,
  `titanium` int(11) NOT NULL,
  `iron` int(11) NOT NULL,
  `ship` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `bankdebit` int(11) NOT NULL default '0',
  `totaluserdebit` int(11) NOT NULL default '0',
  `alphaship` int(3) NOT NULL default '0',
  `betaship` int(3) NOT NULL default '0',
  `deltaship` int(3) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `username`, `password`, `email`, `date`, `created`, `population`, `credits`, `energy`, `darkmatter`, `titanium`, `iron`, `ship`, `city`, `bankdebit`, `totaluserdebit`, `alphaship`, `betaship`, `deltaship`) VALUES
(4, 'Donal', 'O Domhnaill', 'hammerheadzoid', '5f4dcc3b5aa765d61d8327deb882cf99', '', '2010-10-19', '2013-10-19 14:16:08', 1000, 2659, 18, 21, 2, 2, 2, 11, 14193, 14432, 1, 0, 1),
(7, 'Linda', 'Reilly', 'bind', 'f150776d32dc1e993ace8020e01f75c9', '', '2010-11-27', '2010-11-27 01:42:47', 100, 35000, 5, 20, 4, 2, 1, 1, 0, 0, 1, 0, 1),
(13, 'Richard', 'Russell', 'stranger', 'b6281b62d763ff8af75d905dd5c0cdb0', '', '2011-01-01', '2011-01-01 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(14, 'sean', 'o domhnaill', 'johnnyfireball', 'c263fe815cf98d6e8ea5095adae9f503', '', '2011-01-02', '2011-01-02 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(15, 'Wallace', 'Jones', 'wallace', '953f2a1ec16efa816d7505148c2208b9', '', '2011-01-02', '2011-01-02 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(16, 'Ryan', 'Walker', 'crwalker', 'fb160fcf6f085a8da8ec92d2ed807507', '', '2011-01-02', '2011-01-02 22:03:33', 100, 574, 5, 20, 3, 2, 1, 1, 1074, 1074, 1, 0, 0),
(17, 'amanda', 'smith', 'asmith', '5f4dcc3b5aa765d61d8327deb882cf99', 'smith@itworks.ie', '2011-01-27', '2011-01-27 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(18, 'Mark', 'Aherne', 'faerdan', '5f4dcc3b5aa765d61d8327deb882cf99', 'faerdan@gmail.com', '2011-01-27', '2011-01-27 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(19, 'Andrew', 'Dewdney ', 'armour', 'bc124978ded2cf4da7cd25a6465e61e6', 'andrew@armour.ie', '2011-02-05', '2011-02-05 07:51:05', 100, 1979, 5, 20, 2, 2, 1, 1, -22, 422, 1, 0, 0),
(20, 'thingy', 'asdf', 'name', '5f4dcc3b5aa765d61d8327deb882cf99', 'asdf@sdf.ie', '2011-02-07', '2011-02-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(21, 'Jamie', 'Casey', 'hespy', 'e8750ee348987fead337a9928f238a79', 'jamiekc@gmail.com', '2011-02-07', '2011-02-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(22, 'God', 'Almighty', 'god', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'some@thing.com', '2011-02-07', '2011-02-07 16:44:12', 100, 95, 5, 20, 2, 2, 1, 1, -1905, 2305, 1, 0, 0),
(23, 'Mart', 'ODonnell', 'atheos', '0d107d09f5bbe40cade3de5c71e9e9b7', 'martinod@gmail.com', '2011-03-18', '2011-03-18 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(24, 'Accuffelerius', 'Accuffelerius', 'accuffelerius', '27fa77b668b5af3bc1b6a5f54aee3fa5', 'b.orizzzy@gmail.com', '2011-05-02', '2011-05-01 23:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(25, 'chivers', 'jam', 'strawberry', '5f4dcc3b5aa765d61d8327deb882cf99', 'donalod@gmail.com', '2012-03-07', '2012-03-07 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(26, 'John', 'Doe', 'johndoe', '5f4dcc3b5aa765d61d8327deb882cf99', 'nothanks', '2012-03-12', '2012-03-12 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(27, 'DoctorFranko', 'DoctorFrankoED', 'doctorfranko', 'cca457c541ed69d735611fed2522b388', 'docrorleonel776@gmail.com', '2013-02-18', '2013-02-18 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(28, 'DoctorFederic', 'DoctorFedericZA', 'doctorfederic', '06a960a95853eb9816ff2d1f983aaf98', 'doctorfederic@gmail.com', '2013-02-19', '2013-02-19 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(29, 'LeonelDoctor', 'LeonelDoctorPD', 'leoneldoctor', 'bd374eb6791a8b06c89db58c31ce338e', 'leoneldoctorsks7@gmail.com', '2013-02-20', '2013-02-20 00:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0),
(30, 'cigioclop', 'cigioclopLM', 'cigioclop', '11061ffa0983aec3bcea630195c69a62', 'wroxerserge@hotmail.com', '2013-05-13', '2013-05-12 23:00:00', 100, 2000, 5, 20, 2, 2, 1, 1, 0, 0, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `worlds`
--

CREATE TABLE IF NOT EXISTS `worlds` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) unsigned NOT NULL,
  `tile1` varchar(20) default 'grass',
  `tile2` varchar(20) default 'grass',
  `tile3` varchar(20) default 'grass',
  `tile4` varchar(20) default 'grass',
  `tile5` varchar(20) default 'grass',
  `tile6` varchar(20) default 'grass',
  `tile7` varchar(20) default 'grass',
  `tile8` varchar(20) default 'grass',
  `tile9` varchar(20) default 'grass',
  `tile10` varchar(20) default 'grass',
  `tile11` varchar(20) default 'grass',
  `tile12` varchar(20) default 'grass',
  `tile13` varchar(20) default 'grass',
  `tile14` varchar(20) default 'grass',
  `tile15` varchar(20) default 'grass',
  `tile16` varchar(20) default 'grass',
  `tile17` varchar(20) default 'grass',
  `tile18` varchar(20) default 'grass',
  `tile19` varchar(20) default 'grass',
  `tile20` varchar(20) default 'grass',
  `tile21` varchar(20) default 'grass',
  `tile22` varchar(20) default 'grass',
  `tile23` varchar(20) default 'grass',
  `tile24` varchar(20) default 'grass',
  `tile25` varchar(20) default 'grass',
  `tile26` varchar(20) default 'grass',
  `tile27` varchar(20) default 'grass',
  `tile28` varchar(20) default 'grass',
  `tile29` varchar(20) default 'grass',
  `tile30` varchar(20) default 'grass',
  `tile31` varchar(20) default 'grass',
  `tile32` varchar(20) default 'grass',
  `tile33` varchar(20) default 'grass',
  `tile34` varchar(20) default 'grass',
  `tile35` varchar(20) default 'grass',
  `tile36` varchar(20) default 'grass',
  `tile37` varchar(20) default 'house',
  `tile38` varchar(20) default 'grass',
  `tile39` varchar(20) default 'grass',
  `tile40` varchar(20) default 'grass',
  `tile41` varchar(20) default 'grass',
  `tile42` varchar(20) default 'grass',
  `tile43` varchar(20) default 'grass',
  `tile44` varchar(20) default 'grass',
  `tile45` varchar(20) default 'grass',
  `tile46` varchar(20) default 'grass',
  `tile47` varchar(20) default 'grass',
  `tile48` varchar(20) default 'grass',
  `tile49` varchar(20) default 'grass',
  `tile50` varchar(20) default 'grass',
  `tile51` varchar(20) default 'grass',
  `tile52` varchar(20) default 'grass',
  `tile53` varchar(20) default 'grass',
  `tile54` varchar(20) default 'grass',
  `tile55` varchar(20) default 'grass',
  `tile56` varchar(20) default 'grass',
  `tile57` varchar(20) default 'grass',
  `tile58` varchar(20) default 'grass',
  `tile59` varchar(20) default 'grass',
  `tile60` varchar(20) default 'grass',
  `tile61` varchar(20) default 'grass',
  `tile62` varchar(20) default 'grass',
  `tile63` varchar(20) default 'grass',
  `tile64` varchar(20) default 'grass',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `worlds`
--

INSERT INTO `worlds` (`id`, `user_id`, `tile1`, `tile2`, `tile3`, `tile4`, `tile5`, `tile6`, `tile7`, `tile8`, `tile9`, `tile10`, `tile11`, `tile12`, `tile13`, `tile14`, `tile15`, `tile16`, `tile17`, `tile18`, `tile19`, `tile20`, `tile21`, `tile22`, `tile23`, `tile24`, `tile25`, `tile26`, `tile27`, `tile28`, `tile29`, `tile30`, `tile31`, `tile32`, `tile33`, `tile34`, `tile35`, `tile36`, `tile37`, `tile38`, `tile39`, `tile40`, `tile41`, `tile42`, `tile43`, `tile44`, `tile45`, `tile46`, `tile47`, `tile48`, `tile49`, `tile50`, `tile51`, `tile52`, `tile53`, `tile54`, `tile55`, `tile56`, `tile57`, `tile58`, `tile59`, `tile60`, `tile61`, `tile62`, `tile63`, `tile64`) VALUES
(2, 4, 'wall', 'barracks', 'house', 'barracks', 'wall', 'house', 'barracks', 'house', 'playground', 'playground', 'playground', 'house', 'house', 'grass', 'grass', 'house', 'shield1', 'grass', 'grass', 'airporttop', 'grass', 'grass', 'house', 'grass', 'grass', 'grass', 'barracks', 'airportbot', 'grass', 'house', 'barracks', 'grass', 'grass', 'barracks', 'grass', 'grass', 'house', 'wall', 'house', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'playground', 'house', 'grass', 'playground', 'grass', 'house', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'house', 'house', 'playground'),
(3, 17, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'house', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass'),
(4, 25, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass'),
(5, 26, 'grass', 'grass', 'grass', 'barracks', 'barracks', 'house', 'grass', 'grass', 'grass', 'grass', 'grass', 'house', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'playground', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'house', 'grass'),
(6, 27, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass'),
(7, 28, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass'),
(8, 29, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass'),
(9, 30, 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass', 'grass');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
