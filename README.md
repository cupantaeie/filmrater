# README #

This project, filmrater, is still in the very early stages of development. The odds are that the name of the project will change over time but at the moment it describes accurately what the project does. When the application, or website, is complete users will be able to do the following (using the "Must have, Should have, Could have, and Would like but won't get" method):

Must Have
=========
* Login /Log out
* Search for a films details on IMDB (using an open source API called omdbapi.com)
* Review that film for themselves (the film MUST be found through omdbapi)
* Other user will be able to see who has commented on that film

Should Have
===========
* Ability to edit your review
* (For version 2 - an apk)
* (For version 3 - an iOS app)

Could Have
==========
* Ability to leave a comment on the film

Would like but won't get
========================
* Groups - ability to create groups for you to review films with a specific bunch of friends
* My own custom web scraper as I am using omdbapi and this company could close at any time.



### What is this repository for? ###
This repository will store all of the files connected to the filmrater project. 
Version 0.0.1

### Who do I talk to? ###

If you have any questions please email Donal at donalod@gmail.com