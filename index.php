<?php
/*
 * http://img.omdbapi.com/?apikey=[yourkey]&
 * So rambo is http://www.omdbapi.com/?t=rambo&y=&plot=short&r=json
 * Parameter 	Required    Valid Options 			Default Value 	Description
 * =========	========	=============			=============	===========
 * i			Optional*							<empty>			A valid IMDb ID (e.g. tt1285016)
 * t			Optional*							<empty>			Movie title to search for.
 * type 		No			movie, series, episode	<empty>			Type of result to return.
 * y			No									<empty>			Year of release.
 * plot			No			short, full				short			Return short or full plot.
 * r			No			json, xml				json			The data type to return.
 * tomatoes		No			true, false				false			Include Rotten Tomatoes ratings.
 * callback		No									<empty>			JSONP callback name.
 * v			No									1				API version (reserved for future use).
 */

// Turn On/Off Debug
$debug = true;
if (true === $debug) {
	ini_set('display_errors', 0); // 1 = show errors; 0 = no error shown
	error_reporting(E_ALL);
}
//start a session so that the user can be tracked on the computer
session_start(); 

//$_SESSION['successfullLogin'] = "False";

include("info.php");  // This holds the database details
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="filmrater.css" />
	<title>Film Rater - Main Page</title>
</head>

<body>
<div id="container">
<div id="userInfoContainer">
	
	<?php

		if($_SESSION['username'])
		{
			echo "Welcom back ".$_SESSION['username'];
		}
		else
		{
			echo 'Username is NOT set';
		}
	?>

	<form action="ajax/contact.php" method="post" class="ajax">
		<input type="text" name="username" placeholder="Username" class="inputForm" id="loginUsername">
		<input type="text" name="password" placeholder="Password" class="inputForm" id="loginPassword">
		<input type="submit" value="Log in" class="inputForm" id="signInButton">
	</form>

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>

	<?php
		if($_SESSION['successfullLogin'] == "True")
		{
			?>
			<script type="text/javascript">
				document.getElementById('loginUsername').style.display = 'none';
				document.getElementById('loginPassword').style.display = 'none';
				document.getElementById('signInButton').style.display = 'none';
				console.log("I am in the DOM");
			</script>
			<?php
		}
	?>
</div>	



<?php
/******************************************************
*
* 	GET FILM - Welcome to filmrater
*
******************************************************/
?>

<h1>Welcome to filmrater, the website where you rate films for your friends!</h1>

<form action="index.php">
  Find a film now!<br>
  <input type="text" name="name" method="get" placeholder="Enter film name">
  <br>
  <br>
  <input type="submit" value="Find Film/TV Series">
</form>

<?php

/******************************************************
*
* 	var_dump to hide
*
******************************************************/

// When we start we want the screen to be nice and clean. If the user 
// has not searched anything yet then the variable rawtitle will be empty. 

//unset($rawtitle);

if (!$_GET["name"])
{
	echo "There is no film chosen so we will display - Jaws!";
	 $_GET["name"] = "Jaws";
}

$rawtitle = $_GET["name"];							// Take in the name using get
$rawtitle= str_replace(' ','+',$rawtitle);			// replace spaces with +

// Put "rawtitle" in query and get json result back
$jsonurl = file_get_contents("http://www.omdbapi.com/?t=".$rawtitle."&y=&plot=short&r=json");
$_POST[jsonchunk] = $jsonurl;						// Store data for insertion into database
$resulturl = json_decode($jsonurl, true);

//var_dump($resulturl);								// See what I am getting from rawtitle

// Display the info, that is in rawtitle, to the user here...
echo "<div id='filmInfoContainer'>";

if (empty($rawtitle)) {
    echo '<br>Please enter a film in the text box above.';
}
else
{
	echo "<h2>".$resulturl['Title']." (".$resulturl['Year'].")</h2>";
	if ('N/A' == $resulturl['Director'])
	{
		echo "<br>Directed by : Many Directors.";
	}
	else
	{
		echo "<br>Directed by ".$resulturl['Director'];
	}
	echo "<br>Starring : ".$resulturl['Actors'];
	echo "<br>IMDB gives it ".$resulturl['imdbRating'];
	echo "<br>Runtime is ".$resulturl['Runtime'];
	echo "<br>Written by ".$resulturl['Writer'];
	echo "<br>Plot Summary & Opinion : ".$resulturl['Plot']." You might like it if you are into the ".$resulturl['Genre']." genre.";
	echo "<br><img src='".$resulturl['Poster']."'>";
	echo "<br>Awards: ".$resulturl['Awards'];
}
echo "</div>";

/******************************************************
*
* 	SUBMIT FILM INTO THE DATABASE
*
******************************************************/
$_POST[filmtitle] = $resulturl['Title'];			// For the purposes of adding into the db alone


echo "<br><br><h1>Rate and Submit this film</h1>";


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "filmrater";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

//connect_db(); // Open the connection to the db...
?>

<form name="checkin.php" method="post">
	<input type="submit" name="submit" value="Submit to my filmrater account!">
</form>

<?php
if ($_POST['submit'])
{
	$sql = "INSERT INTO filmrater (film_title,json_chunk) VALUES ('$_POST[filmtitle]','$_POST[jsonchunk]')";
	if ($conn->query($sql) === TRUE) 
	{
    	echo "<br>The film was successfully added to your filmrater account.";
   	} 
   	else 
   	{
	    echo "Error: " . $sql . "<br>" . $conn->error;
	}
}

$conn->close();























































/*

// Connect to the database - Make new mysqli object
$mysqli = new mysqli($serverHost, $serverUserName, $serverPassWord, $serverDataBase);
if (mysqli_connect_errno())
{
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
else
{
	echo "</br><i>Connected sucessfully to db!</i></br>";
}

// Set name for this test... will be entered by the user anymore
$name = "Hammerhead";

// This is the actual query that I am feeding into the query object.
$query = "SELECT * FROM filmrater WHERE username = '".$name."'";
$result = $mysqli->query($query);
echo "Hello There";
var_dump($result);
echo "Hello World";

/* numeric array */
//$row = $result->fetch_array(MYSQLI_NUM);
//printf ("%s,%s,%s,%s\n", $row[0], $row[1], $row[2], $row[3]);
/*
$nom1 = $row[0];
$nom2 = $row[1];
$nom3 = $row[2];
$nom4 = $row[3];
$nom5 = $row[4];
$nom6 = $row[5];
$nom7 = $row[6];
$nom8 = $row[7];

echo $nom1."<br>";
echo $nom2."<br>";
echo $nom3."<br>";
echo $nom4."<br>";
echo $nom5."<br>";
echo $nom6."<br>";
echo $nom7."<br>";
echo $nom8."<br>";

/*
//echo "<img src=".$resulturl2['Poster']."/>";
echo "<br><br>Variable is : ".$rawtitle."<br>";
unset($rawtitle);
echo "Variable is unset : ".$rawtitle."<br>";
*/
?>

<h2></h2>

<p id="demo"></p>


</div>
</body>
</html>
