<?php
//start a session so that the user can be tracked on the computer
session_start(); 

// This holds the database details
include("info.php");
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="filmrater.css" />
	<title>Oscars 2015</title>
</head>

<body>
<div id="container">	
	<IMG class="displayed" src="oscars-2014-nominees.png" alt="Oscars!">
	<h1>Welcome to your personal Oscar nominations page!</h1> 
	<p>Enter your name (so we can refer to you) and your email (so we can tell you who won) and continue onto the next page!</p>
	<p>The Oscars are on the 16th February 2016. Keep tuned into this page coming up to the Oscars nominations date to cast your vote as to who you think will win. Do you have what it takes to be a judge? When the results are given out the winning results will be compared to yours and you can see how you did!</p>
	<p>Good luck Oscar Judge,</p>
	<p>Hammerhead!</p>

	<div id="formOne">
	<h1>Register Your Name:</h1>
		<form name="contactForm" action='checkin.php' method='POST'>
			Name:<input type="text" size="20" value="Enter Your Name" onClick="if(this.value == 'Enter Your Name'){ this.value = '';}" name="nameField"></br>
			E-Mail:<input type="text" size="20" value="Enter Your Email" onClick="if(this.value == 'Enter Your Email'){ this.value = '';}" name="emailField"></br>
			<input type='submit' name='submit' value='Register'></input>
		</form>
	</div>
	<div id="formTwo">
	<h1>Check Your Nominations:</h1>
		<form name="contactForm" action='view.php' method='post'>
			Name:<input type="text" size="20" value="Enter Your Name" onClick="if(this.value == 'Enter Your Name'){ this.value = '';}" name="fname"></br></br>
			<input type='submit' name='login' value='View Your Choices'></input>
		</form>
	</div>
	<hr>
	<div id="footer">Donal O Domhnaill | email - donalod@gmail.com</div>
</div>
</body>
</html>